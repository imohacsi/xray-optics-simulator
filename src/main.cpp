#include <iostream>

#include "./config/config.hpp"
#include "./config/geometry.hpp"
#include "./elements/optics.hpp"
#include "./pipeline/pipeline.hpp"


using namespace std;

int main(int argc,char *argv[]){

	/**Default configuration from .ini files**/
	std::string inifile("./input/basicconfig.ini");
	std::string geofile("./input/geometry.ini");
	if(argc>1)  inifile = argv[1];
	if(argc>2)  geofile = argv[2];



    /**Reading simulation parameters from config file**/
    configure conf;
    conf.parse_inifile( inifile );
    simenv env = conf.get_simenv();

    /**Reading simulation geometry from config file**/
    geometry layers;
    layers.parse_layersfile( geofile );
    std::list<element> optics = layers.get_opt_elements();

    /**Building and running simulation pipeline**/
    pipeline pl( env, optics );
    pl.build_pipeline();
    pl.run_pipeline();

    std::cout << "Hurray! Done with simulations!" << std::endl;
    return 0;
}
