#ifndef __FZPSIM_WAVEFIELD_HPP__
#define __FZPSIM_WAVEFIELD_HPP__

#include <iostream>
#include <complex>
#include <vector>
#include <fftw3.h>

#include "../config/simenv.hpp"

/**The main data container storing a 2D complex wavefield and do basic operations on it.**/
class wavefield: public simenv {
    public:
        int64_t ops_allowed = false;
        /**Constructor taking an environment data as input**/
        wavefield(simenv env): simenv(env){
            //Initialize wavefield
           init_field_flat();
           //Plan Fourier transforms
            plan_fourier_transform();
            //Allow operations on the field
            ops_allowed=true; };

        /**Querries the data pointer for the buffers**/
        std::complex<double>* get_dptr(){ _check_state(); return Field.data(); }
        std::vector<std::complex<double>> get_xsect(){ _check_state(); return xsection_x; }
        std::vector<std::complex<double>> get_ysect(){ _check_state(); return xsection_y; }

        /**Preform actions on the current state**/
        void propagate_wavefront( double );
        void save_cross_sections( );

    protected:
        /**Internal check**/
        void _check_state(){ if(!ops_allowed){ printf("ERROR: Field is uninitialized before operation.\n"); exit(-1); } };
        /**Main data buffers**/
        std::vector<std::complex<double>> Field;
        std::vector<std::complex<double>> xsection_x;
        std::vector<std::complex<double>> xsection_y;
        /**FFTW plans*/
        fftw_plan fftFW,fftBW;
        void plan_fourier_transform();

        /**Methods to initialize the wavefield**/
        void init_cross_section();
        void init_field_flat();
        void init_field_tilt( double );
        void init_field_incoh();

        /**Propagate the angular spectrum**/
        void apply_angular_spectrum(double);
        void fft_norm();
};

#endif // __FZPSIM_WAVEFIELD_HPP__
