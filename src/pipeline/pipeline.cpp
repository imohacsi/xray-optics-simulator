#include <fftw3.h>
#include "pipeline.hpp"

void pipeline::build_pipeline(){
    //Set number of threads for multi threading
    omp_set_num_threads( _env._num_threads );

    //Print memory consumption
    double meminfo=16.0*_env._num_pixels*_env._num_pixels;
    printf("Building simulation pipeline...\n");
    printf("\t%d layers found in geometry config...\n", (int)_opt.size() );
    printf("\t%0.0f MB memory required for primary wavefield.\n",meminfo/(1024.0*1024.0) );
};


void pipeline::run_pipeline(){
    std::list<element>::const_iterator it;
    for(it=_opt.begin(); it!=_opt.end(); it++ ){
        if( it->type=="fzp_basic" ){ run_fzp_basic( *it ); }
        else if( it->type=="fzp_linedoubled" ){ run_fzp_ldoubled( *it ); }
        else if( it->type=="free_space" ){ run_free_space( *it ); }
        else if( it->type=="load_tiff" ){ run_load_tiff( *it ); }
        else if( it->type=="aperture" ){ run_aperture( *it ); }
        else if( it->type=="aerial_image" ){ run_aerial_image( *it ); }
        else if( it->type=="thumb_image" ){ run_thumb_image( *it ); }
        else if( it->type=="sum_int" ){ run_sum_int( *it ); }
        else if( it->type=="xsection_image" ){ run_xsection_image( *it ); }
        else{ printf("WARNING: You seem to have an unknown layer: %s\n", it->type.c_str() ); }
    }
};

/**Apply an on-the-fly generated zone plate on the wavefield**/
void pipeline::run_fzp_basic( element elm ){
    opt_fzp_basic fzp( elm, _env );
    fzp._arm();
//    fzp._print();

    printf("\n");
    for( int64_t ss=0; ss<fzp._num_slices; ss++ ){
        printf("\rCalculating layer %s: %d of%d", fzp.type.c_str(), (int)ss, (int)fzp._num_slices );fflush(stdout);
        fzp._apply_on_field( _wfield.get_dptr(), _env._num_pixels );
        if( fzp._num_slices>1 ){ _wfield.propagate_wavefront( fzp._height_slice ); }
        if( fzp._do_xsection  ){ _wfield.save_cross_sections(); }
    }
}

/**Apply an on-the-fly generated line-doubled zone plate on the wavefield**/
void pipeline::run_fzp_ldoubled( element elm ){
    opt_fzp_ldoubled fzp( elm, _env );
    fzp._arm();
//    fzp._print();

    printf("\n");
    for( int64_t ss=0; ss<fzp._num_slices; ss++ ){
        printf("\rCalculating layer %s: %d of%d", fzp.type.c_str(), (int)ss, (int)fzp._num_slices );fflush(stdout);
        fzp._apply_on_field( _wfield.get_dptr(), _env._num_pixels );
        if( fzp._num_slices>1 ){ _wfield.propagate_wavefront( fzp._height_slice ); }
        if( fzp._do_xsection  ){ _wfield.save_cross_sections(); }
    }
}

/**Load density map from file**/
void pipeline::run_load_tiff( element elm ){
    opt_load_tiff ltiff( elm, _env );
    ltiff._arm();
//    ltiff._print();

    printf("Starting TIFF looping...\n"); fflush(stdout);
    printf("\n");
    for( int64_t ss=0; ss<ltiff._num_slices; ss++ ){
        printf("\rCalculating layer %s: %d of%d", ltiff.type.c_str(), (int)ss, (int)ltiff._num_slices );fflush(stdout);
        ltiff._apply_on_field( _wfield.get_dptr(), _env._num_pixels );
        if( ltiff._num_slices>1 ){ _wfield.propagate_wavefront( ltiff._height_slice ); }
        if( ltiff._do_xsection  ){ _wfield.save_cross_sections(); }
    }
}


/**Propagate the wavefield in the nearfield**/
void pipeline::run_free_space( element elm ){
    opt_free_space frees( elm, _env );
    frees._arm();
//    frees._print();

    printf("\n");
    for( int64_t ss=0; ss<frees._num_slices; ss++ ){
        printf("\rCalculating layer %s: %d of%d", frees.type.c_str(), (int)ss, (int)frees._num_slices );fflush(stdout);
        //frees._apply_on_field( _wfield.get_dptr(), _env._num_pixels );
        _wfield.propagate_wavefront( frees._height_slice );
        if( frees._do_xsection  ){ _wfield.save_cross_sections(); }
    }
}

/**Place an aperture in the optical path**/
void pipeline::run_aperture( element elm ){
    opt_aperture apert( elm, _env );
//    apert._print();
    apert._arm();
    apert._apply_on_field( _wfield.get_dptr(), _env._num_pixels );
}


/**Write the current wavefield**/
void pipeline::run_aerial_image( element elm ){
    opt_aerial_image aimg( elm, _env );
//    aimg._print();
    aimg._arm();
    aimg._apply_on_field( _wfield.get_dptr(), _env._num_pixels );
}

/**Write a center thumbnail from the current wavefield**/
void pipeline::run_thumb_image( element elm ){
    opt_thumb_image timg( elm, _env );
//    timg._print();
    timg._arm();
    timg._apply_on_field( _wfield.get_dptr(), _env._num_pixels );
}

/**Sum up the intensity within a given aperture**/
void pipeline::run_sum_int( element elm ){
    opt_sum_intensity sint( elm, _env );
//    sint._print();
    sint._arm();
    sint._apply_on_field( _wfield.get_dptr(), _env._num_pixels );
}

/**Write saved cross section**/
void pipeline::run_xsection_image( element elm ){
    opt_xsection_image ximg( elm, _env );
//    ximg._print();
    ximg._arm();
    ximg._apply_on_field( _wfield.get_xsect(), _wfield.get_ysect(), _env._num_pixels );
}
