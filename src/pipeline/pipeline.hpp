#ifndef __FZPSIM_PIPELINE_HPP__
#define __FZPSIM_PIPELINE_HPP__

#include <string>
#include <list>
#include "wavefield.hpp"
#include "../config/simenv.hpp"
#include "../config/geometry.hpp"
#include "../elements/optics.hpp"

/**Main simulation pipeline**/
class pipeline {
protected:
    simenv              _env;
    std::list<element>  _opt;
    wavefield           _wfield;

    /**Individual processes for all known optical elements**/
    void run_fzp_basic( element );
    void run_fzp_ldoubled( element );
    void run_load_tiff( element );
    void run_aperture( element );
    void run_free_space( element );
    void run_aerial_image( element );
    void run_thumb_image( element );
    void run_sum_int( element );
    void run_xsection_image( element );

public:
    /**Constructor: takes an environment and a list of optical elements**/
    pipeline( simenv env, std::list<element> opt): _wfield(env) { _env=env; _opt=opt; }

    /**Build and run the simulation pipeline**/
    void build_pipeline();
    void run_pipeline();
};

#endif //__FZPSIM_PIPELINE_HPP__
