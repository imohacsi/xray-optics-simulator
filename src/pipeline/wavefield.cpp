#include <iostream>
#include <complex>
#include <vector>
#include <fftw3.h>
#include <omp.h>
#include "wavefield.hpp"

/**Plan Fourier transforms for propagation**/
void wavefield::plan_fourier_transform(){
    char WiseFile[512];
    sprintf(WiseFile,"./fftwisdom.wis");

    //Load previous FFTW plans if exist
    try{
        fftw_import_wisdom_from_filename(WiseFile);
    }catch(int e){
        printf("INFO: no FFTW wisdom has been found, this might take a bit of time!\n");
    }

    fftw_plan_with_nthreads( _num_threads );
    fftFW=fftw_plan_dft_2d(_num_pixels,_num_pixels,reinterpret_cast<fftw_complex*>(Field.data()),reinterpret_cast<fftw_complex*>(Field.data()),FFTW_FORWARD,FFTW_ESTIMATE);
    fftBW=fftw_plan_dft_2d(_num_pixels,_num_pixels,reinterpret_cast<fftw_complex*>(Field.data()),reinterpret_cast<fftw_complex*>(Field.data()),FFTW_BACKWARD,FFTW_ESTIMATE);
    fftw_export_wisdom_to_filename(WiseFile);
}

/**Initialize a flat, perpendicular illuminating wavefront**/
void wavefield::init_field_flat(){
    Field.resize( _num_pixels*_num_pixels, 1.0);
    }

/**Initialize a flat but tilted illuminating wavefront
    @param deg Tilt angle in degrees.**/
void wavefield::init_field_tilt( double deg=6.0 ){
    Field.resize( _num_pixels*_num_pixels, 1.0);
    //Tilt for "deg" angle
    const std::complex<double> i1(0.0,1.0);
    const double pi = 4.0*atan(1.0);
    double tga = tan(deg/180.0*pi);
    double k0 = 2.0*pi / _lambda_si;

    #pragma omp parallel for
    for(int64_t yy=0; yy<_num_pixels; yy++){
        for(int64_t xx=0; xx<_num_pixels; xx++){
            Field[xx+yy*_num_pixels]*=exp(i1*k0*((double)yy)*_dr_pix*tga);
            }
        }
    }

/**Initialize a flat, wavefront with noisy phase**/
void wavefield::init_field_incoh(){
    std::cout << "WARNING: incoherent illumination is unimplemented." << std::endl;
    init_field_flat();
}

/**FFT normalization with the number of pixels**/
void wavefield::fft_norm( ){
    const double norm=1.0/(double)(_num_pixels*_num_pixels);
    #pragma omp parallel for
    for(int64_t xy=0;xy<_num_pixels*_num_pixels;xy++){ Field[xy]*=norm; }
}

/**Propagate the wavefront using the angular spectrum propagator
    @param dist Propagation distance in SI units.**/
void wavefield::propagate_wavefront( double Zdist ){
    _check_state();
    fftw_execute(fftFW);
    apply_angular_spectrum( Zdist );
    fftw_execute(fftBW);
    fft_norm();
}

/**Multiply with angular spectrum propagator
    @param dist Propagation distance in SI units.**/
void wavefield::apply_angular_spectrum( double Zdist ){
    const double pi=4.0*atan(1.0);
    const std::complex<double> i1(0.0,1.0);
    const std::complex<double> r1(1.0,0.0);
    const double TR=_dr_pix*(double)_num_pixels;
    const double k0=2.0*pi/_lambda_si;
    double QQ;
    int64_t xx,yy;
    std::complex<double> mu;

    //Calculate frequency vector
    std::vector<double> freq_vec(_num_pixels,0);
    #pragma omp parallel for private(QQ)
    for( xx=0; xx<_num_pixels; xx++ ){
        if(xx<_num_pixels/2) { QQ=2.0*pi*((double)xx)/(TR); }
        else{ QQ=-(2.0*pi*(double)(_num_pixels-1-xx))/(TR); }
        freq_vec[xx] = QQ; }

    //Propagate along the axis with the leftover momentum
    #pragma omp parallel for private(xx,mu,QQ)
    for( yy=0; yy<_num_pixels; yy++ ){
        for( xx=0; xx<_num_pixels; xx++){
            QQ = ((freq_vec[xx]*freq_vec[xx])+(freq_vec[yy]*freq_vec[yy]));
            mu = sqrt( r1-(QQ/(k0*k0)) );
            Field[xx+yy*_num_pixels]*=exp(i1*k0*Zdist*mu);
        }
    }
}

/**Saving a pair of cross-sections from the current wavefield**/
void wavefield::save_cross_sections(){
    std::vector<std::complex<double>> sect_x(_num_pixels,0);
    std::vector<std::complex<double>> sect_y(_num_pixels,0);

    int64_t _n_mid = _num_pixels/2;
    #pragma omp parallel for
    for(int64_t xx=0; xx<_num_pixels; xx++){
        sect_x[xx] = Field[xx+_n_mid*_num_pixels]; }
    #pragma omp parallel for
    for(int64_t yy=0; yy<_num_pixels; yy++){
        sect_y[yy] = Field[_n_mid+yy*_num_pixels]; }

    xsection_x.insert( xsection_x.end(), sect_x.begin(), sect_x.end() );
    xsection_y.insert( xsection_y.end(), sect_y.begin(), sect_y.end() );

//            int64_t _n_mid = _num_pixels/2;
//    for(int64_t xx=0; xx<_num_pixels; xx++){
//        xsection_x.push_back( Field[xx+_n_mid*_num_pixels] ); }
//
//    for(int64_t yy=0; yy<_num_pixels; yy++){
//        xsection_y.push_back( Field[_n_mid+yy*_num_pixels] ); }
}

