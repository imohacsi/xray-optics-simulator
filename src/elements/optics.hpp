#ifndef __FZPSIM_OPTICS_HPP__
#define __FZPSIM_OPTICS_HPP__

#include "elements.hpp"
#include "opt/opt_load_tiff.hpp"
#include "opt/opt_fzp_basic.hpp"
#include "opt/opt_fzp_linedoubled.hpp"
#include "opt/opt_aperture.hpp"
#include "opt/opt_free_space.hpp"
#include "red/red_aerial_image.hpp"
#include "red/red_thumb_image.hpp"
#include "red/red_sum_intensity.hpp"
#include "red/red_xsection_image.hpp"

#endif //__FZPSIM_OPT_FZP_BASIC_HPP__
