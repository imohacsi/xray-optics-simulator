#ifndef __FZPSIM_OPT_ELEMENTS_HPP__
#define __FZPSIM_OPT_ELEMENTS_HPP__
#include <string>
#include <list>
#include <omp.h>
#include <complex>
#include "../config/simenv.hpp"

/**General optical element parameters with both fixed parameters and a list of pairs for later specialization**/
class element{
public:
    std::string type;
    double _height_struct = 1e-6;               //Structure height
    double _height_slice  = 1e-6;               //Slice height
    int64_t _num_slices = 1;                    //Number of slices
    int64_t _do_xsection = false;               //Do save cross_section information
    std::complex<double> _n_mat=std::complex<double>(1e-5,1e-6);     //Refractive index of the material
    std::complex<double> _phase_slice;          //Phase of a single slice
    /**Additional configuration option for derived classes**/
    std::list< std::pair<std::string,std::string> > par_dict;

    /**Constructor**/
    element(){ };

    /**Set slice height based on slice number**/
    void set_slice_steps( ){
        _height_slice=_height_struct/std::max((int64_t)1,_num_slices); };

    /**Print layer parameters for debug**/
    void _print(){
        printf("Layer type: %s\n", type.c_str() );
        printf("\tStructure height: %g um\n", 1e6*_height_struct );
        printf("\tSlice height: %g um\n", 1e6*_height_slice );
        printf("\tNumber of slices: %d\n", (int)_num_slices );
        printf("\tSave xsection: %d\n", (int)_do_xsection );
        fflush(stdout);
    }
};

#endif //__FZPSIM_OPT_ELEMENTS_HPP__
