#ifndef __FZPSIM_OPT_APERTURE_HPP__
#define __FZPSIM_OPT_APERTURE_HPP__

#include "../elements.hpp"

/**Multiply wavefront with a binary Fresnel zone plate**/
class opt_aperture: public element,simenv {
    protected:
        /**Custom layer parameters**/
        double _diameter = 100.0e-6;     //Diameter of the zone plate
        double _cen_x_si = 0.0;         //Center position of the zone_plate in X
        double _cen_y_si = 0.0;         //Center position of the zone plate in Y
        double _eps = 1e-16;

        /**Parse internal parameters from inherited buffer**/
        void parse_pardict();

    public:
        /**Default constructor requiring an element and an environment**/
        opt_aperture( element elm, simenv env ): element(elm),simenv(env) { };

        /**Finalize layer and apply operation on the passed wavefield**/
        void _arm(){ parse_pardict(); };
        void _apply_on_field( std::complex<double>*, int64_t );
};

#endif //__FZPSIM_OPT_APERTURE_HPP__

