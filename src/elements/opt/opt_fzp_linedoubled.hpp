#ifndef __FZPSIM_OPT_FZP_LDOUBLED_HPP__
#define __FZPSIM_OPT_FZP_LDOUBLED_HPP__

#include "../elements.hpp"

/**Multiply wavefront with a binary Fresnel zone plate**/
class opt_fzp_ldoubled: public element,simenv {
    protected:
        /**Custom layer parameters**/
        double _diameter = 10.0e-6;     //Diameter of the zone plate
        double _zonewidth = 0.2e-6;     //Smallest zone width of the zone plate
        double _cen_x_si = 0.0;         //Center position of the zone_plate in X
        double _cen_y_si = 0.0;         //Center position of the zone plate in Y
        std::complex<double> _n_mat_sup = 0.0;
        std::complex<double> _phase_sup = 0.0;
        int64_t _num_zones=0;
        std::vector<double> _r_in,_r_out;

        /**Parse internal parameters from inherited buffer**/
        void parse_pardict();
        /**Set the optical constants of a slice **/
        void set_slice_phase();
        void set_zone_border();

    public:
        /**Default constructor requiring an element and an environment**/
        opt_fzp_ldoubled( element elm, simenv env ): element(elm),simenv(env) { };

        /**Finalize layer and apply operation on the passed wavefield**/
        void _arm(){ parse_pardict(); set_slice_steps(); set_slice_phase(); set_zone_border(); };
        void _apply_on_field( std::complex<double>*, int64_t );
};

#endif //__FZPSIM_OPT_FZP_LDOUBLED_HPP__
