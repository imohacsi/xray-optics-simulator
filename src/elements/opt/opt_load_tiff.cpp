#include <iostream>
#include <vector>
#include <complex>

#include "opt_load_tiff.hpp"


/**Parse custom parameters from internal buffer**/
void opt_load_tiff::parse_predict(){
    std::list<std::pair<std::string,std::string>>::const_iterator it;
    for( it=par_dict.begin(); it!=par_dict.end(); it++ ){
        if( it->first=="filename" ){ _file_name = it->second; }
        else{ printf("WARNING: unknown argument %s for load_tiff\n", it->first.c_str() ); }
    }
}

/**Calculates the refractive index of a single slice**/
void opt_load_tiff:: set_slice_phase(){
    const double pi = 4.0*atan(1.0);
    const std::complex<double> i1(0.0,1.0);
    _phase_slice = exp( i1*2.0*pi*_height_slice*_n_mat/_lambda_si );
    }



void opt_load_tiff::load_tiff_pattern( ){
    const std::complex<double> i1(0.0,1.0);
    size_t Nx,Ny;

    /**Reading in the image*/
    char ImName[512];
    sprintf(ImName,"%s",_file_name.c_str());
    double* ImgObject=read_tiff(Nx,Ny,ImName);

    /**The loaded mask is always of the pre-defined size*/
    cout << std::endl << "Loaded image with " << Nx <<" x " << Ny << " pixels, cropping "<< _num_pixels << " from center..." << std::endl;

    _dens_map.resize( _num_pixels*_num_pixels );

    int64_t RX=std::min((int64_t)Nx,_num_pixels);
    int64_t RY=std::min((int64_t)Ny,_num_pixels);

    /**Calculating structure heightmap (scaled between 0 and 1)*/
    #pragma omp parallel for
    for( int64_t xx=-RX/2;xx<RX/2;xx++){
    for( int64_t yy=-RY/2;yy<RY/2;yy++){
            _dens_map[ (yy+_num_pixels/2)+(xx+_num_pixels/2)*_num_pixels]=ImgObject[ (xx+Nx/2)+(yy+Ny/2)*Nx ]/255.0;
    }}
    free(ImgObject);
}


void opt_load_tiff::_apply_on_field( std::complex<double>* Field, int64_t N_pix ){
    #pragma omp parallel for
    for(int64_t idx=0;idx<N_pix*N_pix;idx++){
        Field[idx]*=double(_dens_map[idx])*_phase_slice; }
}
