#include <iostream>
#include <complex>
#include <vector>

#include "opt_aperture.hpp"

#define likely(x)    __builtin_expect (!!(x), 1)
#define unlikely(x)  __builtin_expect (!!(x), 0)
inline double sq(double A){ return A*A; }

/**Parse custom parameters from internal buffer**/
void opt_aperture::parse_pardict(){
    std::list<std::pair<std::string,std::string>>::const_iterator it;
    for( it=par_dict.begin(); it!=par_dict.end(); it++ ){
        if( it->first=="diameter" ){ _diameter = std::stod(it->second); }
        else if( it->first=="cen_x" ){ _cen_x_si = std::stod(it->second); }
        else if( it->first=="cen_y" ){ _cen_y_si = std::stod(it->second); }
        else{ printf("WARNING: unknown argument %s for aperture\n", it->first.c_str() ); }
    }
}


void opt_aperture::_apply_on_field(std::complex<double>* Field, int64_t N_pix ){
//    std::cout << "Calling apply fzp_basic on field" << std::endl;
    if( std::max(_cen_x_si,_cen_y_si)>_dr_pix*(N_pix/2.0) ){
        std::cout << "WARNING: aperture center is probably outside of simulated field!" << std::endl; }

    double position;
    int64_t xx,yy;
    const int64_t _n_mid = N_pix/2;
    const double r_apert = _diameter/2.0;

    #pragma omp parallel for private(position,xx)
    for(yy=0; yy<N_pix; yy++){
        for(xx=0; xx<N_pix; xx++){
            position=sqrt(sq(((double)xx-_n_mid)*_dr_pix-_cen_x_si)+sq(((double)yy-_n_mid)*_dr_pix-_cen_y_si));
            if( likely(position>r_apert) ){ Field[xx+yy*N_pix]*= _eps; }
        }
    }
}


