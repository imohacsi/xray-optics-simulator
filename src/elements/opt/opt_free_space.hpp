#ifndef __FZPSIM_OPT_FREE_SPACE_HPP__
#define __FZPSIM_OPT_FREE_SPACE_HPP__
#include "../elements.hpp"

/**Empty class to perform free space propagation on the wavefront,
    can be easily extended to absorbing material.**/
class opt_free_space: public element,simenv {
    /**There are no special members**/
    public:
        opt_free_space( element elm, simenv env ): element(elm),simenv(env) { };

        /**Finalize layer and apply a dummy operation on the passed wavefield**/
        void _arm(){ set_slice_steps(); };
        void _apply_on_field( std::complex<double>*, int64_t ){};
};

#endif //__FZPSIM_OPT_FREE_SPACE_HPP__
