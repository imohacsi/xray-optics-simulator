#ifndef __FZPSIM_OPT_LOAD_TIFF_HPP__
#define __FZPSIM_OPT_LOAD_TIFF_HPP__
#include <vector>
#include <string>
#include <complex>
#include "../elements.hpp"
#include "../../imageprocessing/Imageprocessing.h"

/**Multiply wavefront with an arbitrary pattern from grayscale TIFF file**/
class opt_load_tiff: public element,simenv {
    protected:
        /**Custom layer parameters**/
        std::string         _file_name;     //Full file name of the image file
        std::vector<float>  _dens_map;      //The density map from the image file

        /**Parse internal parameters from inherited buffer**/
        void parse_predict();
        /**Open TIFF file and load its contents as density map**/
        void load_tiff_pattern( );
        /**Set the optical constants of a slice **/
        void set_slice_phase();

    public:
    opt_load_tiff( element elm, simenv env ): element(elm),simenv(env) { }

    /**Finalize layer and apply operation on the passed wavefield**/
    void _arm( ){ parse_predict(); set_slice_steps(); set_slice_phase(); load_tiff_pattern(); };
    void _apply_on_field( std::complex<double>* Field, int64_t N_pix );
};

#endif //__FZPSIM_OPT_LOAD_TIFF_HPP__
