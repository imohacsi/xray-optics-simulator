#include "opt_ml_mirror.hpp"


class vec2 {
public:
	std::vector<std::complex<double>> _data={0,0};
	std::complex<double>& operator[](std::size_t idx){ return _data[idx]; };
	void set_value(std::complex<double> A,std::complex<double> B){
        _data[0]=A; _data[1]=B; }
};


class mat4 {
public:
    double qq;
	std::vector< std::complex<double> > _data = {0,0,0,0};
	void set_value(std::complex<double> A,std::complex<double> B,std::complex<double> C,std::complex<double> D){
        _data[0]=A; _data[1]=B,_data[2]=C;_data[3]=D; };

	std::complex<double>& operator[](std::size_t idx){ return _data[idx]; };

	std::complex<double> det(){
	    return _data[0]*_data[3]-_data[1]*_data[2]; };

    mat4 inv( ) {
        std::complex<double> NRM=1.0/det();
        mat4 tmp;
        tmp[0]=  NRM*_data[3];
        tmp[1]= -NRM*_data[1];
        tmp[2]= -NRM*_data[2];
        tmp[3]=  NRM*_data[0];
        return tmp;
    }

//    mat4 pow( int N ) {
//        mat4 tmp;
//        tmp._data=this->_data;
//        for(int ii=1; ii<N; ii++) { tmp=tmp*A; }
//        return tmp;
//    }
};



//mat4 operator*(mat4& A, mat4& B){
mat4 operator*(mat4 A, mat4 B){
    mat4 tmp;
    tmp[0] = A[0]*B[0]+A[1]*B[2];
    tmp[1] = A[0]*B[1]+A[1]*B[3];
    tmp[2] = A[2]*B[0]+A[3]*B[2];
    tmp[3] = A[2]*B[1]+A[3]*B[3];
    return tmp;
}

vec2 operator*( mat4& M, vec2& V ){
    vec2 tmp;
    tmp[0]=M[0]*V[0]+M[1]*V[1];
    tmp[1]=M[2]*V[0]+M[3]*V[1];
    return tmp;
}



/**TE mode**/
std::complex<double> opt_ml_mirror::PGS_refl_TE(double q) {

	const std::complex<double> i1(0.0,1.0);
	std::complex<double> r;

	/**Incoming angle**/
	double Phi0=2.0*asin(0.5*q/_k0);

	/**Special cases**/
	if( q>2.0*_k0 ) { return 0.0;	}
	if( !std::isnormal(Phi0) ){ return 0.0;  } //If evanescent waves
	else if( Phi0 > _pi/2    ){ return -1.0; } //If backscattering
	else {
        /**Incoming angles after the capping layer**/
		double the_cap=asin(sin(Phi0)/_n_mat2.real());
		double the_mat1=asin(sin(the_cap)*_n_cap.real()/_n_mat1.real());
		double the_mat2=asin(sin(the_mat1)*_n_mat1.real()/_n_mat2.real());

		std::complex<double> na=1.0*cos(Phi0);
		std::complex<double> nr=_n_cap.real()*cos(the_cap);
		std::complex<double> ns=_n_mat1.real()*cos(the_mat1);
		std::complex<double> nm=_n_mat2.real()*cos(the_mat2);
		std::complex<double> cnrm;

		std::complex<double> cnr=_n_cap*cos(the_cap);
		std::complex<double> cns=_n_mat1*cos(the_mat1);
		std::complex<double> cnm=_n_mat2*cos(the_mat2);

		mat4 TOT,SL,ST;

		/**First calculate the capping layer**/
		mat4 M_aircap,L_cap,M_capmat1,L_mat1,S_cap;
		cnrm=1.0/(2.0*nr);
		M_aircap._data = { cnrm*(na+nr), cnrm*(nr-na), cnrm*(nr-na), cnrm*(na+nr) };
		L_cap.set_value( exp(i1*_k0*cnr*_h_cap), 0.0, 0.0, exp(-i1*_k0*cnr*_h_cap) );
		cnrm=1.0/(2.0*ns);
		M_capmat1.set_value( cnrm*(nr+ns), cnrm*(ns-nr), cnrm*(ns-nr), cnrm*(nr+ns) );
		L_mat1.set_value( exp(i1*_k0*cns*_h_mat1), 0.0, 0.0, exp(-i1*_k0*cns*_h_mat1) );
		S_cap=(L_mat1*M_capmat1)*(L_cap*M_aircap);


		/**Then calculate the multilayer**/
		mat4 M_mat1mat2,L_mat2,M_mat2mat1;
		cnrm=1.0/(2.0*nm);
		M_mat1mat2.set_value( cnrm*(ns+nm), cnrm*(nm-ns), cnrm*(nm-ns), cnrm*(ns+nm)  );
		L_mat2.set_value( exp(i1*_k0*cnm*_h_mat2), 0.0, 0.0, exp(-i1*_k0*cnm*_h_mat2) );
		cnrm=1.0/(2.0*ns);
		M_mat2mat1.set_value( cnrm*(nm+ns), cnrm*(ns-nm), cnrm*(ns-nm), cnrm*(nm+ns)  );
		L_mat1.set_value( exp(i1*_k0*cns*_h_mat1), 0.0, 0.0, exp(-i1*_k0*cns*_h_mat1) );
		SL=(L_mat1*M_mat2mat1)*(L_mat2*M_mat1mat2);

		/**Combine many layers of multilayer**/
		SL=(SL*SL); //pow2
		SL=(SL*SL); //pow4
		SL=(SL*SL); //pow8
		SL=(SL*SL); //pow16
		SL=(SL*SL); //pow32
		SL=(SL*SL); //pow64

		/**Add the capping layer**/
		TOT=SL*S_cap;

		mat4 IST=TOT.inv();
		vec2 TRNS;
		TRNS.set_value( 1.0,0.0 );
		vec2 ExitWave=IST*TRNS;

//complex<double> r=sqrt(norm(ExitWave.data[1])/norm(ExitWave.data[0]))*ExitWave.data[1]/abs(ExitWave.data[1]);

		r=ExitWave._data[1]/ExitWave._data[0];
		if(std::isnormal(norm(r))!=1) { r=0.0; }
	}
	return r;
}



/**TM mode**/
std::complex<double> opt_ml_mirror::PGS_refl_TM(double q) {
	const std::complex<double> i1(0.0,1.0);
	std::complex<double> r;

	/**Incoming angle**/
	double Phi0=2.0*asin(0.5*q/_k0);

	/**Special cases**/
	if(q>2.0*_k0) { return 0.0; }
	if( !std::isnormal(Phi0) ){ return 0.0;  } //If evanescent waves
	else if( Phi0 < _pi/2    ){ return -1.0; } //If backscattering
	else {

        /**Incoming angles after the capping layer**/
		double the_cap=asin(sin(Phi0)/_n_mat2.real());
		double the_mat1=asin(sin(the_cap)*_n_cap.real()/_n_mat1.real());
		double the_mat2=asin(sin(the_mat1)*_n_mat1.real()/_n_mat2.real());


		std::complex<double> na=1.0/cos(Phi0);
		std::complex<double> nr=_n_cap.real()/cos(the_cap);
		std::complex<double> ns=_n_mat1.real()/cos(the_mat1);
		std::complex<double> nm=_n_mat2.real()/cos(the_mat2);
		std::complex<double> cnrm;

		std::complex<double> cnr=_n_cap*cos(the_cap);
		std::complex<double> cns=_n_mat1*cos(the_mat1);
		std::complex<double> cnm=_n_mat2*cos(the_mat2);

		mat4 TOT,SL,ST;

		/**First calculate the capping layer**/
		mat4 M_aircap,L_cap,M_capmat1,L_mat1,S_cap;
		cnrm=cos(Phi0)/(2.0*_n_cap.real());
		M_aircap.set_value( cnrm*(na+nr), cnrm*(nr-na), cnrm*(nr-na), cnrm*(na+nr) );
		L_cap.set_value( exp(i1*_k0*cnr*_h_cap), 0.0, 0.0, exp(-i1*_k0*cnr*_h_cap) );
		cnrm=cos(the_cap)/(2.0*_n_mat1.real());
		M_capmat1.set_value( cnrm*(nr+ns), cnrm*(ns-nr), cnrm*(ns-nr), cnrm*(nr+ns) );
		L_mat1.set_value( exp(i1*_k0*cns*_h_mat1), 0.0, 0.0, exp(-i1*_k0*cns*_h_mat1) );
		S_cap=(L_mat1*M_capmat1)*(L_cap*M_aircap);

		/**Then calculate the multilayer**/
		mat4 M_mat1mat2,L_mat2,M_mat2mat1;
		cnrm=cos(the_mat1)/(2.0*_n_mat2.real());
		M_mat1mat2.set_value( cnrm*(ns+nm), cnrm*(nm-ns), cnrm*(nm-ns), cnrm*(ns+nm) );
		L_mat2.set_value( exp(i1*_k0*cnm*_h_mat2), 0.0, 0.0, exp(-i1*_k0*cnm*_h_mat2)   );
		cnrm=cos(the_mat2)/(2.0*_n_mat1.real());
		M_mat2mat1.set_value( cnrm*(nm+ns), cnrm*(ns-nm), cnrm*(ns-nm), cnrm*(nm+ns)  );
		L_mat1.set_value( exp(i1*_k0*cns*_h_mat1), 0.0, 0.0, exp(-i1*_k0*cns*_h_mat1)   );

		SL=(L_mat1*M_mat2mat1)*(L_mat2*M_mat1mat2);
		SL=(SL*SL); //pow2
		SL=(SL*SL); //pow4
		SL=(SL*SL); //pow8
		SL=(SL*SL); //pow16
		SL=(SL*SL); //pow32
		SL=(SL*SL); //pow64
		TOT=(SL*S_cap);

		mat4 IST=TOT.inv();
		vec2 TRNS;
		TRNS.set_value(1.0,0.0);
		vec2 ExitWave=(IST*TRNS);

//complex<double> r=sqrt(norm(ExitWave.data[1])/norm(ExitWave.data[0]))*ExitWave.data[1]/abs(ExitWave.data[1]);
		r=ExitWave._data[1]/ExitWave._data[0];
		if(std::isnormal(norm(r))!=1) { r=0.0; }
	}
	return r;
}


void opt_ml_mirror::_apply_on_field(std::complex<double> *Field, int64_t N_pix){
    int64_t xx,yy;
    const double TR=_dr_pix*(double)N_pix;
    double Qx,Qy,Qxy;
    printf("\nWARNING: ML mirror has not been debugged! Use it at your owns risk!\n\n"); fflush(stdout);

    #pragma omp parallel for private(xx,Qx,Qy,Qxy)
    for(yy=0;yy<N_pix;yy++){
        if(yy<N_pix/2) {  Qy=2.0*_pi*((double)yy)/(TR);}
        else{ Qy=-(2.0*_pi*(double)(N_pix-1-yy))/(TR);}

        for(xx=0;xx<N_pix;xx++){
            if(xx<N_pix/2) {  Qx=2.0*_pi*((double)xx)/(TR);}
            else{ Qx=-(2.0*_pi*(double)(N_pix-1-xx))/(TR); }
            Qxy=sqrt((Qx*Qx)+(Qy*Qy));
            Field[xx+yy*N_pix]*=0.5*( PGS_refl_TE(Qxy)+PGS_refl_TM(Qxy) );
//           Field[xx+yy*N_pix]*=-1.0;
        }
    }
}

