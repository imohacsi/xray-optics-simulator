#include <iostream>
#include <complex>
#include <vector>

#include "opt_fzp_basic.hpp"

#define likely(x)    __builtin_expect (!!(x), 1)
#define unlikely(x)  __builtin_expect (!!(x), 0)
inline double sq(double A){ return A*A; }

/**Parse custom parameters from internal buffer**/
void opt_fzp_basic::parse_pardict(){
    std::list<std::pair<std::string,std::string>>::const_iterator it;
    for( it=par_dict.begin(); it!=par_dict.end(); it++ ){
        if( it->first=="diameter" ){ _diameter = std::stod(it->second); }
        else if( it->first=="zonewidth" ){ _zonewidth = std::stod(it->second); }
        else if( it->first=="cen_x" ){ _cen_x_si = std::stod(it->second); }
        else if( it->first=="cen_y" ){ _cen_y_si = std::stod(it->second); }
        else{ printf("WARNING: unknown argument %s for fzp_basic\n", it->first.c_str() ); }
    }
}

/**Calculates the refractive index of a single slice**/
void opt_fzp_basic:: set_slice_phase(){
    const double pi = 4.0*atan(1.0);
    const std::complex<double> i1(0.0,1.0);
    _phase_slice = exp( i1*2.0*pi*_height_slice*_n_mat/_lambda_si );
    }


void opt_fzp_basic::set_zone_border(){
    double foc=_diameter*_zonewidth/_lambda_si;
    _num_zones=floor(_diameter/(4.0*_zonewidth));

    /**Generating inner and outer zone boundaries**/
    for(int64_t rr=0; rr<_num_zones/2; rr++){
        _r_out.push_back( sqrt((2.0*rr+2)*foc*_lambda_si) );
        _r_in.push_back(  sqrt((2.0*rr+1)*foc*_lambda_si) );
        //std::cout << R_out.back()/_dr_pix << std::endl;
    }
}

void opt_fzp_basic::_apply_on_field(std::complex<double>* Field, int64_t N_pix ){
//    std::cout << "Calling apply fzp_basic on field" << std::endl;
    if( std::max(_cen_x_si,_cen_y_si)>_dr_pix*(N_pix/2.0) ){
        std::cout << "WARNING: fzp_basic is probably outside of simulated field!" << std::endl; }

    double position;
    int64_t xx,yy,rr;
    const int64_t _n_mid = N_pix/2;

    #pragma omp parallel for private(position,rr,xx)
    for(yy=0; yy<N_pix; yy++){
    for(xx=0; xx<N_pix; xx++){
        position=sqrt(sq(((double)xx-_n_mid)*_dr_pix-_cen_x_si)+sq(((double)yy-_n_mid)*_dr_pix-_cen_y_si));
        for(rr=0;rr<_num_zones/2;rr++){
            if( unlikely(position<_r_out[rr] && position>_r_in[rr]) ){
                    Field[xx+yy*N_pix]*=_phase_slice;
                }
            }
        }
    }
}

