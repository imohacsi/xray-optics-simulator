#ifndef __FZPSIM_OPT_ML_MIRROR_HPP__
#define __FZPSIM_OPT_ML_MIRROR_HPP__
#include <vector>
#include <complex>
#include "../elements.hpp"

/**Multiply wavefront with a binary Fresnel zone plate**/
class opt_ml_mirror: public element,simenv {
    protected:
        /**Custom layer parameters**/
        double _pi = 4.0*atan(1.0);
        double _k0;
        int64_t _num_periods=8;
        double _h_mat1 = 2.28e-9;
        double _h_mat2 = 4.64e-9;
        double _h_cap  = 2.50e-9;
        std::complex<double> _n_mat1 = std::complex<double>( 0.00100206619, 0.00182653288 );
        std::complex<double> _n_mat2 = std::complex<double>( 0.07621834430, 0.00643728115 );
        std::complex<double> _n_cap  = std::complex<double>( 0.11365842800, 0.01707241130 );

        std::complex<double> PGS_refl_TE(double q);
        std::complex<double> PGS_refl_TM(double q);


        /**Parse internal parameters from inherited buffer**/
        void parse_pardict();
        void set_defaults(){ _k0 = 2*_pi/_lambda_si; }

    public:
        /**Default constructor requiring an element and an environment**/
        opt_ml_mirror( element elm, simenv env ): element(elm),simenv(env) { };

        /**Finalize layer and apply operation on the passed wavefield**/
        void _arm(){ parse_pardict(); set_defaults(); };
        void _apply_on_field( std::complex<double>*, int64_t );
};

#endif //__FZPSIM_OPT_ML_MIRROR_HPP__

