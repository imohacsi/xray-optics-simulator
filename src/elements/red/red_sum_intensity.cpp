#include <iostream>
#include <vector>

#include "red_sum_intensity.hpp"
#include "../../imageprocessing/Imageprocessing.h"


void opt_sum_intensity::parse_pardict(){

    std::list<std::pair<std::string,std::string>>::const_iterator it;
    for( it=par_dict.begin(); it!=par_dict.end(); it++ ){
        if( it->first=="path" ){ _path = (it->second); }
        else if( it->first=="filename" ){ _filename = (it->second); }
        else if( it->first=="osa_pixels" ){ _osa_pixels = std::stoi(it->second); }
        else{ printf("WARNING: unknown argument %s for sum_intensity\n", it->first.c_str() ); }
    }
    _osa_pixels = std::min(_osa_pixels,_num_pixels);
}


void opt_sum_intensity::_apply_on_field( std::complex<double>* Field, int64_t N_pix ){

    //Copying center of the field to a buffer
    std::vector<std::complex<double>> buffer(_osa_pixels*_osa_pixels,0);
    int64_t mid_f = N_pix/2;
    int64_t mid_t = _osa_pixels/2;

    double sum_int =0.0;

    #pragma omp parallel for reduction(+: sum_int)
    for( int64_t yy=-mid_t; yy<mid_t;yy++){
        for( int64_t xx=-mid_t; xx<mid_t;xx++){
            sum_int += norm( Field[ (xx+mid_f)+(yy+mid_f)*_num_pixels ] );
        }
    }




    std::cout << "Intensity in aperture area: " << sum_int << std::endl;
    std::string dfilename = _path + "/" +" _filename_totint.dat";
    FILE *fp=nullptr;

    fp = fopen( dfilename.c_str(), "w" );
    fprintf(fp,"total_intensity\t%g\n",sum_int);
    fclose(fp);
}


