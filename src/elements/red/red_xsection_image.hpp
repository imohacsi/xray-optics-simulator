#ifndef __FZPSIM_OPT_XSECTION_IMAGE_HPP__
#define __FZPSIM_OPT_XSECTION_IMAGE_HPP__
#include <complex>
#include <vector>
#include "../elements.hpp"

/**Print the contents of the cross-section buffer to file**/
class opt_xsection_image: public element,simenv {
    protected:
        /**Custom layer parameters**/
        std::string _path = "./Results";
        std::string _filename = "xsection";
        std::string _scaling = "log";

        /**Parse internal parameters from inherited buffer**/
        void parse_pardict();

    public:
        opt_xsection_image( element elm, simenv env ): element(elm),simenv(env) { _scaling="lin"; };

        /**Finalize layer and apply operation on the passed wavefield**/
        void _arm(){ parse_pardict(); };
        void _apply_on_field( std::vector<std::complex<double>>, std::vector<std::complex<double>>, int64_t );
};

#endif //__FZPSIM_OPT_XSECTION_IMAGE_HPP__
