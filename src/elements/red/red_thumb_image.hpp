#ifndef __FZPSIM_OPT_THUMB_IMAGE_HPP__
#define __FZPSIM_OPT_THUMB_IMAGE_HPP__
#include "../elements.hpp"

/**Print an thumbnail image of the current wavefield to file**/
class opt_thumb_image: public element,simenv {
    protected:
        /**Custom layer parameters**/
        int64_t  _thumb_pixels=1024;            //Number of pixels in the thumbnail
        std::string _path = "./Results";        //Folder for data storage
        std::string _filename = "thumb";        //File prefix
        std::string _scaling = "lin";           //Linear or logarithmic scaling for images
        /**Parse internal parameters from inherited buffer**/
        void parse_pardict();

    public:
        opt_thumb_image( element elm, simenv env ): element(elm),simenv(env) { };

        /**Finalize layer and apply operation on the passed wavefield**/
        void _arm(){ parse_pardict(); };
        void _apply_on_field( std::complex<double>*, int64_t );
};

#endif //__FZPSIM_OPT_THUMB_IMAGE_HPP__
