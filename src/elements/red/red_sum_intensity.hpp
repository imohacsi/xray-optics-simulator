#ifndef __FZPSIM_OPT_SUM_INT_HPP__
#define __FZPSIM_OPT_SUM_INT_HPP__
#include "../elements.hpp"

/**Print an thumbnail image of the current wavefield to file**/
class opt_sum_intensity: public element,simenv {
    protected:
        /**Custom layer parameters**/
        int64_t  _osa_pixels=256;            //Number of pixels in the thumbnail
        std::string _path = "./Results";        //Folder for data storage
        std::string _filename = "intensity";        //File prefix
        /**Parse internal parameters from inherited buffer**/
        void parse_pardict();

    public:
        opt_sum_intensity( element elm, simenv env ): element(elm),simenv(env) { };

        /**Finalize layer and apply operation on the passed wavefield**/
        void _arm(){ parse_pardict(); };
        void _apply_on_field( std::complex<double>*, int64_t );
};

#endif //__FZPSIM_OPT_SUM_INT_HPP__

