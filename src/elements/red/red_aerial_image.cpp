#include <iostream>
#include "red_aerial_image.hpp"
#include "../../imageprocessing/Imageprocessing.h"


void opt_aerial_image::parse_pardict(){

    std::list<std::pair<std::string,std::string>>::const_iterator it;
    for( it=par_dict.begin(); it!=par_dict.end(); it++ ){
        if( it->first=="path" ){ _path = (it->second); }
        else if( it->first=="filename" ){ _filename = (it->second); }
        else if( it->first=="scaling" ){ _scaling = (it->second); }
        else{ printf("WARNING: unknown argument %s for aerial_image\n", it->first.c_str() ); }
    }
}

void opt_aerial_image::_apply_on_field( std::complex<double>* Field, int64_t N_pix ){
    std::cout << "Calling apply aerial_image on field" << std::endl;
    std::string imagename;
    imagename = _path + "/" + _filename + "_int.tif";
    std::cout << "Writing aerial image to: " << imagename << std::endl;
    WriteImOut(Field,N_pix,N_pix,_scaling.c_str(),"int","TIFF", imagename.c_str() );
    imagename = _path + "/" + _filename + "_arg.tif";
    std::cout << "Writing aerial image to: " << imagename << std::endl;
    WriteImOut(Field,N_pix,N_pix,"lin","arg","TIFF", imagename.c_str() );
}
