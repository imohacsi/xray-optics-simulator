#include <iostream>
#include <vector>

#include "red_thumb_image.hpp"
#include "../../imageprocessing/Imageprocessing.h"


void opt_thumb_image::parse_pardict(){

    std::list<std::pair<std::string,std::string>>::const_iterator it;
    for( it=par_dict.begin(); it!=par_dict.end(); it++ ){
        if( it->first=="path" ){ _path = (it->second); }
        else if( it->first=="filename" ){ _filename = (it->second); }
        else if( it->first=="scaling" ){ _scaling = (it->second); }
        else if( it->first=="thumb_pixels" ){ _thumb_pixels = std::stoi(it->second); }
        else{ printf("WARNING: unknown argument %s for aerial_image\n", it->first.c_str() ); }
    }
    _thumb_pixels = std::min(_thumb_pixels,_num_pixels);
}


void opt_thumb_image::_apply_on_field( std::complex<double>* Field, int64_t N_pix ){

    //Copying center of the field to a buffer
    std::vector<std::complex<double>> buffer(_thumb_pixels*_thumb_pixels,0);
    int64_t mid_f = N_pix/2;
    int64_t mid_t = _thumb_pixels/2;
    for( int64_t yy=-mid_t; yy<mid_t;yy++){
        for( int64_t xx=-mid_t; xx<mid_t;xx++){
            buffer[ (xx+mid_t)+(yy+mid_t)*_thumb_pixels ] = Field[ (xx+mid_f)+(yy+mid_f)*_num_pixels];
        }
    }

    std::cout << "Calling apply aerial_image on field" << std::endl;
    std::string imagename;
    imagename = _path + "/" + _filename + "_int.tif";
    std::cout << "Writing aerial image to: " << imagename << std::endl;
    WriteImOut(buffer.data(),_thumb_pixels,_thumb_pixels,_scaling.c_str(),"int","TIFF", imagename.c_str() );
    imagename = _path + "/" + _filename + "_arg.tif";
    std::cout << "Writing aerial image to: " << imagename << std::endl;
    WriteImOut(buffer.data(),_thumb_pixels,_thumb_pixels,"lin","arg","TIFF", imagename.c_str() );
}

