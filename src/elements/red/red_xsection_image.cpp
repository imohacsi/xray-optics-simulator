#include <iostream>

#include "red_xsection_image.hpp"
#include "../../imageprocessing/Imageprocessing.h"

void opt_xsection_image::parse_pardict(){
    std::list<std::pair<std::string,std::string>>::const_iterator it;
    for( it=par_dict.begin(); it!=par_dict.end(); it++ ){
        if( it->first=="path" ){ _path = (it->second); }
        else if( it->first=="filename" ){ _filename = (it->second); }
        else if( it->first=="scaling" ){ _scaling = (it->second); }
        else{ printf("WARNING: unknown argument %s for aerial_image\n", it->first.c_str() ); }
    }
}

void opt_xsection_image::_apply_on_field( std::vector<complex<double>> xsect_x, std::vector<complex<double>> xsect_y, int64_t N_pix ){
    std::cout << "Calling apply aerial_image on field" << std::endl;
    std::string imagename;
    int64_t n_len = xsect_x.size()/N_pix;
    if( n_len<1 ){
        printf("WARNING: Cross-section buffer is empty.\n"); fflush(stdout);
        return; }
    std::cout << "Image size: " << n_len << std::endl;

    imagename = _path + "/" + _filename + "_xsect_int.tif";
    std::cout << "Writing aerial image to: " << imagename << std::endl;
    WriteImOut(xsect_x.data(),N_pix,n_len,_scaling.c_str(),"int","TIFF", imagename.c_str() );
    imagename = _path + "/" + _filename + "_xsect_arg.tif";
    std::cout << "Writing aerial image to: " << imagename << std::endl;
    WriteImOut(xsect_x.data(),N_pix,n_len,"lin","arg","TIFF", imagename.c_str() );

    imagename = _path + "/" + _filename + "_ysect_int.tif";
    std::cout << "Writing aerial image to: " << imagename << std::endl;
    WriteImOut(xsect_y.data(),N_pix,n_len,_scaling.c_str(),"int","TIFF", imagename.c_str() );
    imagename = _path + "/" + _filename + "_ysect_arg.tif";
    std::cout << "Writing aerial image to: " << imagename << std::endl;
    WriteImOut(xsect_y.data(),N_pix,n_len,"lin","arg","TIFF", imagename.c_str() );
}

