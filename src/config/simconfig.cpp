#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include "config.hpp"

/**Main configuration file loader, that is completely hand written
*   to parse an input file in ".ini" file format. Unspecified
*   parameters are left alone and not overwritten.
*   @param filename
*/
void configure::parse_inifile(std::string filename) {
	/**Check if file exists**/
	std::ifstream confile(filename);
	if( confile.fail() ) {
		printf( "ERROR: initialization file %s not found, exiting.\n",filename.c_str() );
		fflush(stdout);
		exit(EXIT_FAILURE);
	}

	std::string cline;
	std::string category;
	char h_cat=91;  //Special characer "["
	char h_com=35;  //Special character "#"

	std::string stmp;

	/**Reading and parsing the config file line by line**/
	while (std::getline(confile,cline)) {
		if(!cline.empty()) {
			std::istringstream instr (cline);

			if( cline[0]==h_cat ) {
				instr >> category;
				category.erase(0,1);
				category.pop_back();
			} else if(cline[0]==h_com ) {
				continue;
			} else {
				parse_iniline(cline,category);
			}
		}
	}/**Until EoF*/

	printf("Done config file parsing...\n");
	fflush(stdout);
}


/**Function to parse a single line of input**/
void configure::parse_iniline(std::string cline,std::string category) {
	std::string parameter_name,equal_sign;
	std::istringstream instr (cline);
	instr >> parameter_name >> equal_sign;

	/**Reconstruction parameters*/
	if(category=="simulation") {
		if(parameter_name=="run_name") { instr >> env._run_name; }
		else if(parameter_name=="num_threads") { instr >> env._num_threads; }
		else if(parameter_name=="num_pixels")  { instr >> env._num_pixels;  }
		else if(parameter_name=="pixel_size")  { instr >> env._dr_pix; 		}
		else if(parameter_name=="energy_ev") { instr >> env._energy_ev; env._lambda_si = 1.0e-9*1240.0/env._energy_ev; }
	}
}
