#ifndef __FZPSIM_SIMENV_HPP__
#define __FZPSIM_SIMENV_HPP__

/**Stores the key simulation parameters**/
class simenv{
public:
    std::string _run_name;

    int64_t _num_threads;           //Number of threads for simulations
    int64_t _num_pixels;            //Number of pixels in the simulation (key factor for memory requirement)

    double  _dr_pix;                //Pixel size of the simulation
    double _lambda_si;             //Simulation wavelength
    double _energy_ev;             //Simulation energy

protected:
    void set_defaults(){
        _num_threads = 4;
        _num_pixels = 2048;
        _dr_pix = 20e-9;
        _lambda_si = 0.2e-9;
        _energy_ev = 6200.0;
        }
};

#endif //__FZPSIM_SIMENV_HPP__
