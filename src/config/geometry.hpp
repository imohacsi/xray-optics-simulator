#ifndef __FZPSIM_GEOMETRY_HPP__
#define __FZPSIM_GEOMETRY_HPP__
#include <string>
#include <list>
#include "simenv.hpp"
#include "../elements/elements.hpp"

/**Reads a list of optical elements from a config file to determine the geometry.**/
class geometry{
protected:
    simenv env;
    void parse_iniline(std::string,std::string,element*);
    std::list<element> opt_elements;

public:
    void parse_layersfile(std::string);
    std::list<element> get_opt_elements(){ return opt_elements; }
};
#endif //__FZPSIM_GEOMETRY_HPP__
