#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "geometry.hpp"

std::ifstream open_fstream( std::string filename ){
	/**Check if file exists**/
	std::ifstream confile(filename);
	if( confile.fail() ) {
		printf( "ERROR: initialization file %s not found, exiting.\n",filename.c_str() );
		fflush(stdout);
		exit(EXIT_FAILURE);
	}
    return confile;
}


void geometry::parse_layersfile(std::string filename) {
	/**Check if file exists**/
	std::ifstream confile = open_fstream(filename);

	std::string cline;
	std::string category("null");
    std::string oldcategory;
	char h_cat=91;  //Special characer "["
	char h_com=35;  //Special character "#"

	std::string stmp;
	int64_t is_first=1;
	element* curr_opt_element;

	/**Reading and parsing the config file line by line**/
	while (std::getline(confile,cline)) {
		if(!cline.empty()) {
			std::istringstream instr (cline);
            /**Found new category**/
			if( cline[0]==h_cat ) {
				instr >> category;
				category.erase(0,1);
				category.pop_back();

				/**Reset buffers**/
                if( !is_first ){ opt_elements.push_back( *curr_opt_element ); }
                is_first = 0;
                curr_opt_element = new element;
                curr_opt_element->type = category;

            /**Commented line**/
			} else if(cline[0]==h_com ) {
				continue;
            /**Regular parameter line**/
			} else {
				parse_iniline(cline,category,curr_opt_element);
			}
		}
	}/**Until EoF*/

	printf("Done geometry file parsing... (found %d layers)\n", (int)opt_elements.size() );
	fflush(stdout);
}


/**Function to parse a single line of input**/
void geometry::parse_iniline( std::string cline, std::string category, element* elm ) {
	std::string parameter_name,equal_sign;
    std::pair<std::string,std::string> entry;
	std::istringstream instr (cline);
	instr >> parameter_name >> equal_sign;

	/**Reconstruction parameters*/
    elm->type = category;
    if(parameter_name=="height_struct") {
        instr >> elm->_height_struct;
    }else if(parameter_name=="n_mat") {
        double tmp1,tmp2;
        instr >> tmp1 >> tmp2;
        elm->_n_mat = std::complex<double>(tmp1, tmp2);
    }else if(parameter_name=="num_slices") {
        instr >> elm->_num_slices;
    }else if(parameter_name=="do_save_xsection") {
        instr >> elm->_do_xsection;
    }else{
        entry.first = parameter_name;
        instr >> entry.second;
        elm->par_dict.push_back( entry );
    }
}

