#ifndef __FZPSIM_CONFIG_HPP__
#define __FZPSIM_CONFIG_HPP__
#include "simenv.hpp"

/**Reads the key simulation parameters from config file**/
class configure{
protected:
    simenv env;
    void parse_iniline(std::string,std::string);

public:
    void parse_inifile(std::string);
    simenv get_simenv(){ return env; }
};
#endif //__FZPSIM_CONFIG_HPP__
