#ifndef __IMAGEPROCESSING_CPP__
#define __IMAGEPROCESSING_CPP__

#include <cstring>
#include <stdio.h>

#include "Imageprocessing.h"
#define pi 4.0*atan(1.0)


double Imaximum(float* data, int N){
double maximum=data[0];
for(int i=0;i<N;i++){ if((data[i]-maximum)>0.0){maximum=data[i];} }
return maximum;
}

double Imaximum(double* data, int N){
double maximum=data[0];
for(int i=0;i<N;i++){ if((data[i]-maximum)>0.0){maximum=data[i];} }
return maximum;
}

double Imaximum(complex<double>* data, int N){
double maximum=norm(data[0]);
for(int i=0;i<N;i++){ if((norm(data[i])-maximum)>0.0){maximum=norm(data[i]);} }
return maximum;
}

double Imaximum(complex<float>* data, int N){
double maximum=norm(data[0]);
for(int i=0;i<N;i++){ if((norm(data[i])-maximum)>0.0){maximum=norm(data[i]);} }
return maximum;
}

double Iminimum(float* data, int N){
double minimum=data[0];
for(int i=0;i<N;i++){ if((data[i]-minimum)<0.0){minimum=data[i];} }
return minimum;
}

double Iminimum(double* data, int N){
double minimum=data[0];
for(int i=0;i<N;i++){ if((data[i]-minimum)<0.0){minimum=data[i];} }
return minimum;
}

double Iminimum(complex<double>* data, int N){
double minimum=norm(data[0]);
for(int i=0;i<N;i++){ if((norm(data[i])-minimum)<0.0){minimum=norm(data[i]);} }
return minimum;
}

double Iminimum(complex<float>* data, int N){
double minimum=norm(data[0]);
for(int i=0;i<N;i++){ if((norm(data[i])-minimum)<0.0){minimum=norm(data[i]);} }
return minimum;
}

/***********************************************/
/***********************************************/

void WriteImOut(complex<double> *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename){
//    printf("NI %d %d\n",Nx,Ny); fflush(stdout);
double maxval=Imaximum(data,Nx*Ny);
double minval=Iminimum(data,Nx*Ny);
    printf("Image scaled: %g\t%g\n",minval,maxval); fflush(stdout);
double normfact;
if(strcmp(format,"BMP")==0){ normfact=255.0; }
if(strcmp(format,"TIFF")==0){ normfact=256.0*256.0-1; }

double *dpmap=new double[Nx*Ny];



if(strcmp(mode,"lin")==0){
    if(strcmp(par,"int")==0){
            #pragma omp parallel for
            for(int ii=0;ii<Nx*Ny;ii++){
                dpmap[ii]=normfact*norm(data[ii])/maxval;
    }}
    if(strcmp(par,"dint")==0){
            #pragma omp parallel for
            for(int ii=0;ii<Nx*Ny;ii++){
                dpmap[ii]=normfact*(norm(data[ii])-minval)/(maxval-minval);
    }}
    if(strcmp(par,"arg")==0){
        #pragma omp parallel for
            for(int ii=0;ii<Nx*Ny;ii++){
                dpmap[ii]=normfact*(arg(data[ii])+pi)/(2.0*pi);
    }}
}

if(strcmp(mode,"log")==0){
    if(strcmp(par,"int")==0){
            if(minval<1e-6){ minval=1e-6; }
            #pragma omp parallel for
            for(int ii=0;ii<Nx*Ny;ii++){
                dpmap[ii]=normfact*(log(norm(data[ii])/minval)/log(maxval/minval));;
                }
    }}

if(strcmp(format,"BMP")==0){ write_bitmap(dpmap,Nx,Ny,filename); }
if(strcmp(format,"TIFF")==0){ write_tiff(dpmap,Nx,Ny,filename); }

delete [] dpmap;
}


void WriteImOut(complex<float> *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename){

float maxval=(float)Imaximum(data,Nx*Ny);
float minval=(float)Iminimum(data,Nx*Ny);
if(minval<=0.0f){minval=1e-16f;}

float normfact;
if(strcmp(format,"BMP")==0){ normfact=255.0f; }
if(strcmp(format,"TIFF")==0){ normfact=(float)(256.0*256.0-1.0); }

float *fpmap=new float[Nx*Ny];

if(strcmp(mode,"lin")==0){
    if(strcmp(par,"int")==0){
            for(int ii=0;ii<Nx*Ny;ii++){
                fpmap[ii]=normfact*norm(data[ii])/maxval;
    }}
    if(strcmp(par,"arg")==0){
            for(int ii=0;ii<Nx*Ny;ii++){
                fpmap[ii]=normfact*(arg(data[ii])+pi)/(2.0*pi);
    }}
}

if(strcmp(mode,"log")==0){
    if(strcmp(par,"int")==0){
            for(int ii=0;ii<Nx*Ny;ii++){
                fpmap[ii]=normfact*(logf(norm(data[ii])/minval)/logf(maxval/minval));;
                }
    }}

if(strcmp(format,"BMP")==0){ write_bitmap(fpmap,Nx,Ny,filename); }
if(strcmp(format,"TIFF")==0){ write_tiff(fpmap,Nx,Ny,filename); }

delete [] fpmap;
}


void WriteImOut(int *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename){

double *dpmap=new double[Nx*Ny];
for(int i=0;i<Nx*Ny;i++){ dpmap[i]=(double)data[i]; }

double maxval=Imaximum(dpmap,Nx*Ny);
double minval=Iminimum(dpmap,Nx*Ny);
if(minval<=0.0){minval=1e-16;}

double normfact;
if(strcmp(format,"BMP")==0){ normfact=255.0; }
if(strcmp(format,"TIFF")==0){ normfact=256.0*256.0-1; }

if(strcmp(mode,"lin")==0){
    if(strcmp(par,"int")==0){
            for(int ii=0;ii<Nx*Ny;ii++){
                dpmap[ii]=normfact*(dpmap[ii]/maxval);
    }}
}

if(strcmp(mode,"log")==0){
    if(strcmp(par,"int")==0){
            for(int ii=0;ii<Nx*Ny;ii++){
                dpmap[ii]=normfact*(log(abs(dpmap[ii])/minval)/log(maxval/minval));;
                }
    }}

if(strcmp(format,"BMP")==0){ write_bitmap(dpmap,Nx,Ny,filename); }
if(strcmp(format,"TIFF")==0){ write_tiff(dpmap,Nx,Ny,filename); }

delete [] dpmap;
}

void WriteImOut(double *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename){

double maxval=Imaximum(data,Nx*Ny);
double minval=Iminimum(data,Nx*Ny);
if(minval<=0.0){minval=1e-16;}

double normfact;
if(strcmp(format,"BMP")==0){ normfact=255.0; }
if(strcmp(format,"TIFF")==0){ normfact=256.0*256.0-1; }

double *dpmap=new double[Nx*Ny];

if(strcmp(mode,"lin")==0){
    if(strcmp(par,"int")==0){
            for(int ii=0;ii<Nx*Ny;ii++){
                dpmap[ii]=normfact*(data[ii]/maxval);
    }}
}

if(strcmp(mode,"log")==0){
    if(strcmp(par,"int")==0){
            for(int ii=0;ii<Nx*Ny;ii++){
                dpmap[ii]=normfact*(log(abs(data[ii])/minval)/log(maxval/minval));;
                }
    }}

if(strcmp(format,"BMP")==0){ write_bitmap(dpmap,Nx,Ny,filename); }
if(strcmp(format,"TIFF")==0){ write_tiff(dpmap,Nx,Ny,filename); }

delete [] dpmap;
}



void WriteImOut(float *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename){

double maxval=Imaximum(data,Nx*Ny);
double minval=Iminimum(data,Nx*Ny);
if(minval<=0.0){minval=0.1;}

double normfact;
if(strcmp(format,"BMP")==0){ normfact=255.0; }
if(strcmp(format,"TIFF")==0){ normfact=256.0*256.0-1; }

double *dpmap=new double[Nx*Ny];

if(strcmp(mode,"lin")==0){
    if(strcmp(par,"int")==0){
            for(int ii=0;ii<Nx*Ny;ii++){
                dpmap[ii]=normfact*(data[ii]/maxval);
    }}
}

if(strcmp(mode,"log")==0){
    if(strcmp(par,"int")==0){
            for(int ii=0;ii<Nx*Ny;ii++){
                dpmap[ii]=normfact*(log(abs(data[ii])/minval)/log(maxval/minval));;
                }
    }}

if(strcmp(format,"BMP")==0){ write_bitmap(dpmap,Nx,Ny,filename); }
if(strcmp(format,"TIFF")==0){ write_tiff(dpmap,Nx,Ny,filename); }

delete [] dpmap;
}





void WriteROIOut(complex<double> *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename, int Ncrop){

complex<double> *cmap=new complex<double>[(Nx-2*Ncrop)*(Ny-2*Ncrop)];

for(int i=Ncrop;i<Nx-Ncrop;i++){
for(int j=Ncrop;j<Ny-Ncrop;j++){
	cmap[(j-Ncrop)+(Nx-2*Ncrop)*(i-Ncrop)]=data[j+Nx*i];
}}

WriteImOut(cmap,Nx-2*Ncrop,Ny-2*Ncrop,mode,par,format,filename);


delete [] cmap;
}

void WriteImNSC(double *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename){
double *dpmap; dpmap=(double*)calloc(Nx*Ny,sizeof(double));
if(strcmp(mode,"lin")==0){
    if(strcmp(par,"int")==0){
            for(int ii=0;ii<Nx*Ny;ii++){
                if(data[ii]<65536.0){
                dpmap[ii]=(double)data[ii];}
                else{ dpmap[ii]=65535.0; }
    }}
}

if(strcmp(format,"TIFF")==0){ write_tiff(dpmap,Nx,Ny,filename); }
free(dpmap);
}




#endif

