#ifndef IO_H
#define IO_H

#include <stdlib.h>
#include <cstring>
#include <cmath>

#define FAILURE 0

using namespace std;

typedef unsigned int uint32;
typedef unsigned char uint8;
typedef unsigned short int uint16;

/**
 * Read a tiff file. Returns a 2D of the data.
 *
 * @param file_name The name of the file to read from
 * @param data The array to be filled with data
 */
double *read_tiff( size_t& N_x, size_t& N_y, char *file_name );


/**
 * Write a 2D array to a tiff file. The data will be saved as a 16 bit
 * grey-scale image. Please note that the data will be scaled to fit
 * within the range 0 - 2^16. Hence this method is only useful for
 * viewing the final output and should not be used if you plan to
 * reopen the file at a later date and pass the data back to the
 * reconstruction algorithm.
 *
 * @param file_name The name of the file to write to
 * @param data The array to be written to file
 * @param log_scale Output on log scale? true/false. Default is false.
 */
int write_tiff(float *data, int N_x, int N_y, const char *file_name );
int write_tiff(double *data, int N_x, int N_y, const char *file_name );

#endif

