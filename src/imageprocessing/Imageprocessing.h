#ifndef __IMAGEPROCESSING_H__
#define __IMAGEPROCESSING_H__

#include "bitmap.h"
#include "tiff.h"

/**Ancient left-over C library from early PhD**/

double Imaximum(double* data, int N);
double Imaximum(complex<double>* data, int N);
double Iminimum(double* data, int N);
double Iminimum(complex<double>* data, int N);
void WriteImOut(complex<double> *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename);
void WriteImOut(complex<float> *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename);
void WriteImOut(double *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename);
void WriteImOut(float *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename);
void WriteImOut(int *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename);
void WriteROIOut(complex<double> *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename, int Ncrop);
void WriteImNSC(double *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename);


#endif
