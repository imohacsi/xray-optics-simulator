#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Imageprocessing.h"

#define DEFAULT_DPI_X 3780
#define DEFAULT_DPI_Y 3780
#define DPI_FACTOR 39.37007874015748
#define FALSE (0)
#define TRUE !FALSE


void bmp_create_standard_color_table(bmpfile_t *bmp){
  int i, j, k, ell;

  switch (bmp->dib.depth) {
  case 1:
    for (i = 0; i < 2; ++i) {
      bmp->colors[i].red = i * 255;
      bmp->colors[i].green = i * 255;
      bmp->colors[i].blue = i * 255;
      bmp->colors[i].alpha = 0;
    }
    break;

  case 4:
    i = 0;
    for (ell = 0; ell < 2; ++ell) {
      for (k = 0; k < 2; ++k) {
	for (j = 0; j < 2; ++j) {
	  bmp->colors[i].red = j * 128;
	  bmp->colors[i].green = k * 128;
	  bmp->colors[i].blue = ell * 128;
	  bmp->colors[i].alpha = 0;
	  ++i;
	}
      }
    }

    for (ell = 0; ell < 2; ++ell) {
      for (k = 0; k < 2; ++k) {
	for (j = 0; j < 2; ++j) {
	  bmp->colors[i].red = j * 255;
	  bmp->colors[i].green = k * 255;
	  bmp->colors[i].blue = ell * 255;
	  bmp->colors[i].alpha = 0;
	  ++i;
	}
      }
    }

    i = 8;
    bmp->colors[i].red = 192;
    bmp->colors[i].green = 192;
    bmp->colors[i].blue = 192;
    bmp->colors[i].alpha = 0;

    break;

  case 8:
    i = 0;
    for (ell = 0; ell < 4; ++ell) {
      for (k = 0; k < 8; ++k) {
	for (j = 0; j < 8; ++j) {
	  bmp->colors[i].red = j * 32;
	  bmp->colors[i].green = k * 32;
	  bmp->colors[i].blue = ell * 64;
	  bmp->colors[i].alpha = 0;
	  ++i;
	}
      }
    }

    i = 0;
    for (ell = 0; ell < 2; ++ell) {
      for (k = 0; k < 2; ++k) {
	for (j = 0; j < 2; ++j) {
	  bmp->colors[i].red = j * 128;
	  bmp->colors[i].green = k * 128;
	  bmp->colors[i].blue = ell * 128;
	  ++i;
	}
      }
    }

    // overwrite colors 7, 8, 9
    i = 7;
    bmp->colors[i].red = 192;
    bmp->colors[i].green = 192;
    bmp->colors[i].blue = 192;
    i++; // 8
    bmp->colors[i].red = 192;
    bmp->colors[i].green = 220;
    bmp->colors[i].blue = 192;
    i++; // 9
    bmp->colors[i].red = 166;
    bmp->colors[i].green = 202;
    bmp->colors[i].blue = 240;

    // overwrite colors 246 to 255
    i = 246;
    bmp->colors[i].red = 255;
    bmp->colors[i].green = 251;
    bmp->colors[i].blue = 240;
    i++; // 247
    bmp->colors[i].red = 160;
    bmp->colors[i].green = 160;
    bmp->colors[i].blue = 164;
    i++; // 248
    bmp->colors[i].red = 128;
    bmp->colors[i].green = 128;
    bmp->colors[i].blue = 128;
    i++; // 249
    bmp->colors[i].red = 255;
    bmp->colors[i].green = 0;
    bmp->colors[i].blue = 0;
    i++; // 250
    bmp->colors[i].red = 0;
    bmp->colors[i].green = 255;
    bmp->colors[i].blue = 0;
    i++; // 251
    bmp->colors[i].red = 255;
    bmp->colors[i].green = 255;
    bmp->colors[i].blue = 0;
    i++; // 252
    bmp->colors[i].red = 0;
    bmp->colors[i].green = 0;
    bmp->colors[i].blue = 255;
    i++; // 253
    bmp->colors[i].red = 255;
    bmp->colors[i].green = 0;
    bmp->colors[i].blue = 255;
    i++; // 254
    bmp->colors[i].red = 0;
    bmp->colors[i].green = 255;
    bmp->colors[i].blue = 255;
    i++; // 255
    bmp->colors[i].red = 255;
    bmp->colors[i].green = 255;
    bmp->colors[i].blue = 255;
    break;
  }
}




void bmp_create_grayscale_color_table(bmpfile_t *bmp){
  uchar step_size;

  if (!bmp->colors) return;

  if (bmp->dib.depth != 1)
    step_size = 255 / (bmp->dib.ncolors - 1);
  else
    step_size = 255;

  for (unsigned int i = 0; i < bmp->dib.ncolors; ++i) {
    uchar value = i * step_size;
    rgb_pixel_t color = {value, value, value, 0};
    bmp->colors[i] = color;
  }
}



static void bmp_malloc_colors(bmpfile_t *bmp){
  bmp->dib.ncolors = (int)pow(2.0, bmp->dib.depth);
  if (bmp->dib.depth == 1 || bmp->dib.depth == 4 || bmp->dib.depth == 8) {
    bmp->colors = (rgb_pixel_t*)malloc(sizeof(rgb_pixel_t) * bmp->dib.ncolors);
    bmp_create_standard_color_table(bmp);
  }
}

/** Free the memory of color palette */
static void bmp_free_colors(bmpfile_t *bmp){
  if (bmp->colors)
    free(bmp->colors);
}

/** Malloc the memory for pixels */
static void bmp_malloc_pixels(bmpfile_t *bmp){
  bmp->pixels = (rgb_pixel_t*)malloc(sizeof(rgb_pixel_t *) * bmp->dib.width* bmp->dib.height);
  for (size_t i = 0; i < bmp->dib.width*bmp->dib.height; ++i) {
      bmp->pixels[i].red = 255;
      bmp->pixels[i].green = 255;
      bmp->pixels[i].blue = 255;
      bmp->pixels[i].alpha = 0;
    }
}

/** Free the memory of pixels */
static void bmp_free_pixels(bmpfile_t *bmp){
  free(bmp->pixels);
  bmp->pixels = NULL;
}

/** Create the BMP object with specified width and height and depth.  */
bmpfile_t *bmp_create(uint width, uint height, uint depth){
  bmpfile_t *result;
  double bytes_per_pixel;
  uint bytes_per_line;
  uint palette_size;

  if (depth != 1 && depth != 4 && depth != 8 && depth != 16 && depth != 24 && depth != 32)
    return NULL;

  result =(bmpfile_t*) calloc(sizeof(bmpfile_t),1);

  result->header.magic[0] = 'B';
  result->header.magic[1] = 'M';

  result->dib.header_sz = 40;
  result->dib.width = width;
  result->dib.height = height;
  result->dib.nplanes = 1;
  result->dib.depth = depth;
  result->dib.hres = DEFAULT_DPI_X;
  result->dib.vres = DEFAULT_DPI_Y;

  if (depth == 16)
    result->dib.compress_type = BI_BITFIELDS;
  else
    result->dib.compress_type = BI_RGB;

  bmp_malloc_pixels(result);
  bmp_malloc_colors(result);

  /* Calculate the field value of header and DIB */
  bytes_per_pixel = (result->dib.depth * 1.0) / 8.0;
  bytes_per_line = (int)ceil(bytes_per_pixel * result->dib.width);
  if (bytes_per_line % 4 != 0)
    bytes_per_line += 4 - bytes_per_line % 4;

  result->dib.bmp_bytesz = bytes_per_line * result->dib.height;

  palette_size = 0;
  if (depth == 1 || depth == 4 || depth == 8)
    palette_size = (int)pow(2.0, result->dib.depth) * 4;
  else if (result->dib.depth == 16)
    palette_size = 3 * 4;

  result->header.offset = 14 + result->dib.header_sz + palette_size;
  result->header.filesz = result->header.offset + result->dib.bmp_bytesz;

  return result;
}

void bmp_destroy(bmpfile_t *bmp){
  bmp_free_pixels(bmp);
  bmp_free_colors(bmp);
  free(bmp);
}


bmp_header_t bmp_get_header(bmpfile_t *bmp){
  return bmp->header;
}

void bmp_set_dpi(bmpfile_t *bmp, uint x, uint y){
  bmp->dib.hres = (uint)(x * DPI_FACTOR);
  bmp->dib.vres = (uint)(y * DPI_FACTOR);
}

rgb_pixel_t *bmp_get_pixel(bmpfile_t *bmp, uint x, uint y){
  if ((x >= bmp->dib.width) || (y >= bmp->dib.height))
    return NULL;
  return &(bmp->pixels[x+y*bmp->dib.width]);
}

bool bmp_set_pixel(bmpfile_t *bmp, uint x, uint y, rgb_pixel_t pixel){
  if ((x >= bmp->dib.width) || (y >= bmp->dib.height))
    return FALSE;

  bmp->pixels[x+y*bmp->dib.width] = pixel;
  return TRUE;
}

static void bmp_write_header(bmpfile_t *bmp, FILE *fp){
  bmp_header_t header = bmp->header;
  fwrite(header.magic, sizeof(header.magic), 1, fp);
  fwrite(&(header.filesz), sizeof(uint), 1, fp);
  fwrite(&(header.creator1), sizeof(unsigned short int), 1, fp);
  fwrite(&(header.creator2), sizeof(unsigned short int), 1, fp);
  fwrite(&(header.offset), sizeof(uint), 1, fp);
}

static void bmp_write_dib(bmpfile_t *bmp, FILE *fp){
  bmp_dib_v3_header_t dib = bmp->dib;

  fwrite(&(dib.header_sz), sizeof(uint), 1, fp);
  fwrite(&(dib.width), sizeof(uint), 1, fp);
  fwrite(&(dib.height), sizeof(uint), 1, fp);
  fwrite(&(dib.nplanes), sizeof(unsigned short int), 1, fp);
  fwrite(&(dib.depth), sizeof(unsigned short int), 1, fp);
  fwrite(&(dib.compress_type), sizeof(uint), 1, fp);
  fwrite(&(dib.bmp_bytesz), sizeof(uint), 1, fp);
  fwrite(&(dib.hres), sizeof(uint), 1, fp);
  fwrite(&(dib.vres), sizeof(uint), 1, fp);
  fwrite(&(dib.ncolors), sizeof(uint), 1, fp);
  fwrite(&(dib.nimpcolors), sizeof(uint), 1, fp);
}

static void bmp_write_palette(bmpfile_t *bmp, FILE *fp){
  if (bmp->dib.depth == 1 || bmp->dib.depth == 4 || bmp->dib.depth == 8) {
    for (size_t i = 0; i < bmp->dib.ncolors; ++i){
      fwrite(&(bmp->colors[i]), sizeof(rgb_pixel_t), 1, fp);
    }
  }
  else if (bmp->dib.depth == 16) { /* the bit masks, not palette */
    unsigned short int red_mask = 63488;  /* bits 1-5 */
    unsigned short int green_mask = 2016; /* bits 6-11 */
    unsigned short int blue_mask = 31;    /* bits 12-16 */
    unsigned short int zero_word = 0;

    fwrite(&red_mask, sizeof(unsigned short int), 1, fp);
    fwrite(&zero_word, sizeof(unsigned short int), 1, fp);

    fwrite(&green_mask, sizeof(unsigned short int), 1, fp);
    fwrite(&zero_word, sizeof(unsigned short int), 1, fp);

    fwrite(&blue_mask, sizeof(unsigned short int), 1, fp);
    fwrite(&zero_word, sizeof(unsigned short int), 1, fp);
  }
}

#define INT_SQUARE(v) ((int)((v) * (v)))

//static int find_closest_color(bmpfile_t *bmp, rgb_pixel_t pixel){
//  unsigned int i, best = 0;
//  int best_match = 999999;
//
//  for (i = 0; i < bmp->dib.ncolors; ++i) {
//    rgb_pixel_t color = bmp->colors[i];
//    int temp_match = INT_SQUARE(color.red - pixel.red) +
//      INT_SQUARE(color.green - pixel.green) +
//      INT_SQUARE(color.blue - pixel.blue);
//    if (temp_match < best_match){ best = i; best_match = temp_match; }
//    if (best_match < 1){ break; }
//  }
//  return best;
//}


bool bmp_save(bmpfile_t *bmp, const char *filename){
  FILE *fp;
  /* Create the file */
  if ((fp = fopen(filename, "wb")) == NULL)
    return FALSE;

  /* Write the file */
  bmp_write_header(bmp, fp);
  bmp_write_dib(bmp, fp);
  bmp_write_palette(bmp, fp);

  if (bmp->dib.depth == 16) {
    uint data_bytes = bmp->dib.width * 2;
    uint padding_bytes = 4 - data_bytes % 4;

    for (int j = bmp->dib.height - 1; j >= 0; --j) {
       uchar zero_byte = 0;
       uint write_number = 0;
      for (int i = 0; write_number < data_bytes; ++i, write_number += 2) {
        unsigned short int red = (unsigned short int)(bmp->pixels[i+j*bmp->dib.width].red / 8);
        unsigned short int green = (unsigned short int)(bmp->pixels[i*bmp->dib.width].green / 4);
        unsigned short int blue = (unsigned short int)(bmp->pixels[i*bmp->dib.width].blue / 8);
        unsigned short int value = (red << 11) + (green << 5) + blue;
        fwrite(&value, sizeof(unsigned short int), 1, fp);
      }
      for (write_number = 0; write_number < padding_bytes; ++write_number)
	fwrite(&zero_byte, 1, 1, fp);
    }
  }
  else {
    double bytes_per_pixel;
    int bytes_in_bitmap;
    bytes_per_pixel = (bmp->dib.depth * 1.0) / 8.0;
    bytes_in_bitmap=(int)(bytes_per_pixel*bmp->dib.width*bmp->dib.height);
    fwrite(bmp->pixels, bytes_in_bitmap, 1, fp);
    }


  fclose(fp);

  return TRUE;
}

int write_bitmap(double *data, int Nx, int Ny, const char *filename){
bmpfile_t *ImageOut;
ImageOut=bmp_create(Nx,Ny,32);
bmp_create_grayscale_color_table(ImageOut);
//rgb_pixel_t newcolor={32,64,128,0};
for(int ii=0;ii<Nx;ii++){
    for(int jj=0;jj<Ny;jj++){
        ImageOut->pixels[jj+ii*Ny] = (rgb_pixel_t){(unsigned char)data[jj+ii*Ny] ,(unsigned char)data[jj+ii*Ny] ,(unsigned char)data[jj+ii*Ny] ,0};
    }}
bmp_save(ImageOut,filename);
bmp_destroy(ImageOut);
return 0;
}


int write_bitmap(float *data, int Nx, int Ny, const char *filename){
bmpfile_t *ImageOut;
ImageOut=bmp_create(Nx,Ny,32);
bmp_create_grayscale_color_table(ImageOut);
//rgb_pixel_t newcolor={32,64,128,0};
for(int ii=0;ii<Nx;ii++){
    for(int jj=0;jj<Ny;jj++){
        ImageOut->pixels[jj+ii*Ny] = (rgb_pixel_t){(unsigned char)data[jj+ii*Ny] ,(unsigned char)data[jj+ii*Ny] ,(unsigned char)data[jj+ii*Ny] ,0};
    }}
bmp_save(ImageOut,filename);
bmp_destroy(ImageOut);
return 0;
}






