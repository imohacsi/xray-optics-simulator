#ifndef __bmpfile_h__
#define __bmpfile_h__

#include <complex>
using namespace std;

typedef enum {
  BI_RGB = 0,
  BI_RLE8,
  BI_RLE4,
  BI_BITFIELDS,
  BI_JPEG,
  BI_PNG,
} bmp_compression_method_t;


typedef unsigned int uint;
typedef unsigned char uchar;

typedef struct {
  uchar blue;
  uchar green;
  uchar red;
  uchar alpha;
} rgb_pixel_t;

typedef struct {
  uchar magic[2];   /* the magic number used to identify the BMP file:
			 0x42 0x4D (Hex code points for B and M).
			 The following entries are possible:
			 BM - Windows 3.1x, 95, NT, ... etc
			 BA - OS/2 Bitmap Array
			 CI - OS/2 Color Icon
			 CP - OS/2 Color Pointer
			 IC - OS/2 Icon
			 PT - OS/2 Pointer. */
  uint filesz;    /* the size of the BMP file in bytes */
  unsigned short int creator1;  /* reserved. */
  unsigned short int creator2;  /* reserved. */
  uint offset;    /* the offset, i.e. starting address, of the byte where the bitmap data can be found. */
} bmp_header_t;

typedef struct {
  uint header_sz;     /* the size of this header (40 bytes) */
  uint width;         /* the bitmap width in pixels */
  uint height;        /* the bitmap height in pixels */
  unsigned short int nplanes;       /* the number of color planes being used. Must be set to 1. */
  unsigned short int depth;         /* the number of bits per pixel, which is the color depth of the image. Typical values are 1, 4, 8, 16, 24 and 32. */
  uint compress_type; /* the compression method being used. See also bmp_compression_method_t. */
  uint bmp_bytesz;    /* the image size. This is the size of the raw bitmap data (see below), and should not be confused with the file size. */
  uint hres;          /* the horizontal resolution of the image. (pixel per meter) */
  uint vres;          /* the vertical resolution of the image. (pixel per meter) */
  uint ncolors;       /* the number of colors in the color palette, or 0 to default to 2<sup><i>n</i></sup>. */
  uint nimpcolors;    /* the number of important colors used, or 0 when every color is important; generally ignored. */
} bmp_dib_v3_header_t;

typedef struct _bmpfile bmpfile_t;
struct _bmpfile {
  bmp_header_t header;
  bmp_dib_v3_header_t dib;

  rgb_pixel_t *pixels;
  rgb_pixel_t *colors;
};


bmpfile_t *bmp_create(uint width, uint height, uint depth);
void bmp_destroy(bmpfile_t *bmp);

void bmp_create_grayscale_color_table(bmpfile_t *bmp);
void bmp_create_standard_color_table(bmpfile_t *bmp);
bmp_header_t bmp_get_header(bmpfile_t *bmp);
bmp_dib_v3_header_t bmp_get_dib(bmpfile_t *bmp);

uint bmp_get_width(bmpfile_t *bmp);
uint bmp_get_height(bmpfile_t *bmp);
uint bmp_get_depth(bmpfile_t *bmp);

uint bmp_get_dpi_x(bmpfile_t *bmp);
uint bmp_get_dpi_y(bmpfile_t *bmp);
void bmp_set_dpi(bmpfile_t *bmp, uint x, uint y);

rgb_pixel_t *bmp_get_pixel(bmpfile_t *bmp, uint x, uint y);
bool bmp_set_pixel(bmpfile_t *bmp, uint x, uint y, rgb_pixel_t pixel);

bool bmp_save(bmpfile_t *bmp, const char *filename);

int write_bitmap(double *data, int Nx, int Ny, const char *filename);
int write_bitmap(float *data, int Nx, int Ny, const char *filename);
//void WriteImOut(complex<double> *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename);
//void WriteImOut(double *data, int Nx, int Ny, const char * mode, const char * par, const char *format, const char * filename);


#endif /* __bmpfile_h__ */

