

#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <vector>
#include <sstream>
#include <cmath>
#include <tiffio.h>
#include "Imageprocessing.h"



using namespace std;

#define FAILURE 0
#define SUCCESS 1

#define ONE_BYTE 1
#define TWO_BYTE 2
#define FOUR_BYTE 4

/***************************************************************/
//bad code which allows the array type to be
//decided at run-time.
/***************************************************************/
class tiff_anonymous_array{

  int type_;
  uint8  * a_uint8;
  uint16 * a_uint16;
  uint32 * a_uint32;

 public:

  tiff_anonymous_array(int type, int size){
    type_ = type;

     a_uint8=0;
     a_uint16=0;
     a_uint32=0;

    switch( type ){
    case ONE_BYTE:
      a_uint8 = new uint8[size];
      break;
    case TWO_BYTE:
      a_uint16 = new uint16[size];
      break;
    case FOUR_BYTE:
      a_uint32 = new uint32[size];
      break;
    default:
      cout << "Not familiar with type.. exiting.." << endl;
      exit(1);
    }
  }

  ~tiff_anonymous_array(){
    switch( type_ ){
    case ONE_BYTE:
      delete[] a_uint8;
      break;
    case TWO_BYTE:
      delete[] a_uint16;
      break;
    case FOUR_BYTE:
      delete[] a_uint32;
      break;
    default:
      cout << "Not familiar with the tiff type.. exiting.." << endl;
      exit(1);
    }
  }

  void * return_array(){
    switch( type_ ){
    case ONE_BYTE:
      return a_uint8;
    case TWO_BYTE:
      return a_uint16;
    case FOUR_BYTE:
      return a_uint32;
     default:
      cout << "Not familiar with the tiff type.. exiting.." << endl;
      exit(1);
    }
  }

  double return_array_value(int i){
    //coverting to double. Not ideal.
    switch( type_ ){
    case ONE_BYTE:
      return a_uint8[i];
    case TWO_BYTE:
      return a_uint16[i];
    case FOUR_BYTE:
      return a_uint32[i];
    default:
      cout << "Not familiar with the tiff type.. exiting.." << endl;
      exit(1);
    }
  }
};

/*********************************************************/

/*********************************************************/
double *read_tiff( size_t& N_x, size_t& N_y, char *file_name ){
TIFFSetWarningHandler(NULL);
  //open the input file:
  TIFF* tif = TIFFOpen(file_name, "r");
 // cout<<"File readin\n";
  if (tif) {
    int dircount = 0;
    for( ; TIFFReadDirectory(tif); dircount++);
    if(dircount > 1)
      cout << "Multiple directories in the file "<<file_name
	   << "... only using the first" <<endl;
  }
  else{
    cout << "Could not open the file "<<file_name<<endl;
    return FAILURE;
  }

  N_x=0;    //Size_t type variables must be zeroed!!!
  N_y=0;    //Size_t type variables must be zeroed!!!
  uint16 bits_per_sample;
  uint16 samples_per_pixel;

  TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &N_x);
  TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &N_y);
  TIFFGetField(tif, TIFFTAG_BITSPERSAMPLE, &bits_per_sample);
  TIFFGetField(tif, TIFFTAG_SAMPLESPERPIXEL, &samples_per_pixel);

uint16 sampleformat;
TIFFGetField(tif, TIFFTAG_SAMPLEFORMAT, &sampleformat);
samples_per_pixel = 1;								// NEED TO FIX!!!!!!!!!!!!!

  int strip_size = TIFFStripSize(tif);
  int bytes_per_pixel = bits_per_sample*samples_per_pixel/8;
  int pixels_per_strip = strip_size/bytes_per_pixel;

  double *Data; Data=(double *)calloc(N_x*N_y,sizeof(double));

  if(samples_per_pixel>1){ //see if the image is colour
    cout << "ERROR: Unable to process colour image" << endl;
  }else{ //otherwise if the image is grey scale
    tiff_anonymous_array * buffer;
    switch(bytes_per_pixel){ //otherwise set up the buffer for grey scale
    case(ONE_BYTE):
      buffer = new tiff_anonymous_array(ONE_BYTE,pixels_per_strip);
    break;
    case(TWO_BYTE):
      buffer = new tiff_anonymous_array(TWO_BYTE,pixels_per_strip);
    break;
    case(FOUR_BYTE):
      buffer = new tiff_anonymous_array(FOUR_BYTE,pixels_per_strip);
      break;
    default:
      cout << "Confused about the tiff image.." <<endl;
      return FAILURE;
    }

    for( tstrip_t strip = 0; strip < TIFFNumberOfStrips(tif); strip++){
        size_t bytes_read = (size_t)TIFFReadEncodedStrip(tif, strip, buffer->return_array(), (tsize_t) - 1);
        for( size_t i=0; i< bytes_read/bytes_per_pixel; i++ ){
        Data[i+strip*pixels_per_strip] = (double)buffer->return_array_value(i);
        }}
    }

  TIFFClose(tif);
  return Data; //success
};

//****************************************************************************************************
//int write_tiff(string file_name, int N_x, int N_y, double *data, bool log_scale, double min, double max){
int write_tiff(double *data, int N_x, int N_y, const char *file_name){

  TIFF* tif = TIFFOpen(file_name, "w");
  if (!tif) {
    cout << "Could not open the file "<<file_name<<endl;
    return FAILURE;
  }

  //copy the image into an array
  uint16 * grey_image = new uint16[N_x*N_y];
  for(int i=0; i < N_x; i++){
    for(int j=0; j< N_y; j++){ //copy and scale
      grey_image[j*N_x+i] =(uint16)data[i+N_x*j];
    }
  }
  //	cout<<"writetif_3"<<endl;
  // We need to set some values for basic tags before we can add any data
  TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, N_x);
  TIFFSetField(tif, TIFFTAG_IMAGELENGTH, N_y);
  TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 8*sizeof(uint16));
  TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);
  TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, N_y);

  TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_PACKBITS);
  TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);

	//cout<<"writetif_4"<<endl;
  //write the data out as a single strip
  uint32 bytes_written = TIFFWriteEncodedStrip(tif, 0,(void*)grey_image,N_x*N_y*sizeof(uint16));
  if(bytes_written!=N_x*N_y*sizeof(uint16)){
    cout << "Problem writing to tiff file" << endl;
    exit(1);
  }

	//cout<<"writetif_5"<<endl;
  TIFFClose(tif);
  delete[] grey_image;
  return SUCCESS; //success

};



int write_tiff(float *data, int N_x, int N_y, const char *file_name){

  TIFF* tif = TIFFOpen(file_name, "w");
  if (!tif) {
    cout << "Could not open the file "<<file_name<<endl;
    return FAILURE;
  }

  //copy the image into an array
  uint16 * grey_image = new uint16[N_x*N_y];
  for(int i=0; i < N_x; i++){
    for(int j=0; j< N_y; j++){ //copy and scale
      grey_image[j*N_x+i] =(uint16)data[i+N_x*j];
    }
  }

  // We need to set some values for basic tags before we can add any data
  TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, N_x);
  TIFFSetField(tif, TIFFTAG_IMAGELENGTH, N_y);
  TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 8*sizeof(uint16));
  TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);
  TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, N_y);

  TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_PACKBITS);
  TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);

  //write the data out as a single strip
  uint32 bytes_written = TIFFWriteEncodedStrip(tif, 0,(void*)grey_image,N_x*N_y*sizeof(uint16));
  if(bytes_written!=N_x*N_y*sizeof(uint16)){
    cout << "Problem writing to tiff file" << endl;
    exit(1);
  }

	//cout<<"writetif_5"<<endl;
  TIFFClose(tif);
  delete[] grey_image;
  return SUCCESS; //success

};

