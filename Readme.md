See LICENCE for distribution conditions.

Simulation package for diffractive X-ray optics
by Istvan Mohacsi, Paul Scherrer Institute 2011-2015

This software is distributed without any warranty and was not used in 
any actual publication!

=== Usage information ===

==Basics==

The software was intended to be built on Linux systems using the gcc 
compiler and C++14 or later. It can be operated from the command line 
either by using the default parameter files or by supplying the 
corresponding parameter files as arguments. The first config file sets 
the simulation parameters, the second one sets the geometry.
Example:
./FZP_sim ./input/simconfig.ini ./input/geometry.ini

The config files follow the style of windows ini files and can be 
generated also from script. Results are also written out to files. 
Multiple simulations can be paralellized in a Map-Reduce paradigm.


==Resources==

The main bottleneck for optical simulations is the amount of memory.
The program needs 16*num_pixels^2 bytes of memory to store the entire
2D complex wavefield. Additional memory is needed for reading arbitrary 
patterns from TIFF files.


==Implemented optical elements==

Example config files can be found in the input folder

=Fresnel zone plate=
Binary Fresnel zone plate with 0.5 duty cycle.

height_struct: Zone height
num_slices: Number of slices in non-flat zone plate
diameter: Diameter of the zone plate
zonewidth: Smallest zone of the zone plate 
n_mat: Refractive index of the zone-plate material
cen_x: Zone plate center in x
cen_y: Zone plate center in y
do_save_xsection: save propagation cross-section (default: false)

=Line-doubled Fresnel zone plate
Binary Fresnel zone plate following the "line doubling" fabrication method

height_struct: Zone height
num_slices: Number of slices in non-flat zone plate
diameter: Diameter of the zone plate
zonewidth: Smallest zone of the zone plate
n_mat: Refractive index of the zone-plate material
cen_x: Zone plate center in x
cen_y: Zone plate center in y
n_sup_r: Real part of the support refractive index
n_sup_i: Imaginary part of the support refractive index
do_save_xsection: save propagation cross-section (default: false)

=Load TIFF=
Load an arbitrary pattern from a grayscale image file.
filename: Full path to grayscale TIFF file

=Aperture=
Perfect circular aperture that cuts all intensity outside
diameter: Aperture diameter

=Free space propagation=
Angular spectrum propagator
Parameters:
height_struct: propagation distance
num_slices: number of slices (for cross-sections)
do_save_xsection: save propagation cross-section (default: false)

==Implemented diagnostics==

=Aerial image=
Writes out the entire wavefielt into a large tiff file
path:
prefix:
scaling:

=Thumbnail image=
Writes out a central crop into a tiff file
thumbnail_size:
path:
prefix:
scaling:

=Cross section=
Writes out cross sections to tiff file (if saved)
path: 
prefix:
scaling: "lin" or "log"

=Intensity in aperture=
Measure the total intensity inside an aperture (useful for intensity measurements)
path:
prefix:












